//
//  MTLAppDelegate.h
//  Mantle-JSONAPI
//
//  Created by CocoaPods on 06/04/2015.
//  Copyright (c) 2014 Ricardo Pramana Suranta. All rights reserved.
//

@import UIKit;

@interface MTLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
