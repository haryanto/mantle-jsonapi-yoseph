//
//  main.m
//  Mantle-JSONAPI
//
//  Created by Ricardo Pramana Suranta on 06/04/2015.
//  Copyright (c) 2014 Ricardo Pramana Suranta. All rights reserved.
//

@import UIKit;
#import "MTLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MTLAppDelegate class]));
    }
}
