# Mantle-JSONAPI

[![CI Status](http://img.shields.io/travis/Ricardo Pramana Suranta/Mantle-JSONAPI.svg?style=flat)](https://travis-ci.org/Ricardo Pramana Suranta/Mantle-JSONAPI)
[![Version](https://img.shields.io/cocoapods/v/Mantle-JSONAPI.svg?style=flat)](http://cocoapods.org/pods/Mantle-JSONAPI)
[![License](https://img.shields.io/cocoapods/l/Mantle-JSONAPI.svg?style=flat)](http://cocoapods.org/pods/Mantle-JSONAPI)
[![Platform](https://img.shields.io/cocoapods/p/Mantle-JSONAPI.svg?style=flat)](http://cocoapods.org/pods/Mantle-JSONAPI)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Mantle-JSONAPI is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Mantle-JSONAPI"
```

## Author

Ricardo Pramana Suranta, ricardo@icehousecorp.com

## License

Mantle-JSONAPI is available under the MIT license. See the LICENSE file for more info.
